
[TOC]

## Introduction 

## Quick Data Availability 

**Bioproject:** [PRJNA1115710](https://www.ncbi.nlm.nih.gov/bioproject/1115710)


**Oxford Nanopore Raw Reads:** 


**PacBio HiFi Raw Reads:** 


**Final Genome Assembly:**


**Final Genome Annotation:** 


**Softmasked Genome:**


**Repeats:** 


**Final Transcriptome Assembly:**


**Final Transcriptome Annotation:**


Supplemental Files:


## Final Transcriptome Assembly and Annotation

**Path to Project:** `/core/globus/cgi/RAMP/c_stockmannorum/transcriptome`

- **Assembly:** `c_stock_centroids.fasta`

- **Directory:** `/core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/clustering/`

- **Functional Annotation:** `entap_results.tsv`

- **Directory:** `/core/globus/cgi/RAMP/c_stockmannorum/transcriptome/EnTAP/final_results`


## Final Genome Assembly and Annotation

Directory: `/core/globus/cgi/RAMP/c_stockmannorum`

**Genome Assembly:** `c_stockmannorum_hybrid.fasta`

Directory: `/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/01_genome`

**Genome Functional Annotation:** `blank.gff`

Directory: `/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/03_easel`

**CDS:** `blank.cds`

**Pep:** `blank.pep`

**GFF:** `blank.gff`

**GTF:** `blank.gtf`

# Part I: Chaerilus stockmannorum De Novo Transcriptome Assembly and Annotation

## Assembly 

#### RNA Extraction 

- **Tissue Type:** Booklungs, Central Nervous System, Telson

- **Protocol:** 

#### Model 

- **Flow Cell Type:** 

- **Kit Type:** 

| Reads (before QC) | Reads (after QC) |
|-------------------|------------------|
|          54247246 |         53986188 |
|          37052064 |         36863736 |
|          52310812 |         51618796 |

### Booklungs

- **Full Script:** [nextflow.config](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/assembly/(booklungs)%20nextflow.config?ref_type=heads)

<p>
<details>
<summary>Booklung ORP Script</summary>
<pre><code>
nextflow.enable.dsl=2

process fastp {
  publishDir "$params.outdir/fastp", mode: 'copy'

  module 'fastp/0.23.2'

  memory '30 GB'
  cpus 8
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  input:
  tuple val(id), path(reads)

  output:
  tuple val(id), path('*.fastq.gz'), emit: trimmed
  tuple val(id), path('*.json'), emit: json
  tuple val(id), path('*.html'), emit: html

  script:

  """
  fastp -i ${reads[0]} -I ${reads[1]} -o ${id}_trimmed_1.fastq.gz -O ${id}_trimmed_2.fastq.gz --thread ${task.cpus} --json ${id}.json --html ${id}.html

  """
}
process ORP {
  publishDir "$params.outdir", mode: 'copy'

  memory '150 GB'
  cpus 24
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  container '/core/globus/cgi/RAMP/Scorpion_mRNA_Dec2023/scripts/orp_2.3.3.sif'

  input:
  tuple val(id), path(reads)
  val(lineage)

  output:
  path "${id}.ORP.fasta", emit: orp
  path "*/*", emit: directories

  script:
  """
  source activate orp
  /home/orp/Oyster_River_Protocol/oyster.mk STRAND=RF MEM=150 CPU=24 READ1=${reads[0]} READ2=${reads[1]} RUNOUT=${id} LINEAGE=${lineage}

  """
}
workflow {
  read_pairs_ch = Channel.fromFilePairs(params.reads, checkIfExists: true)
  fastp ( read_pairs_ch )
  ORP ( fastp.out.trimmed, params.lineage )
}

</code></pre>
</details>
</p>

### Central Nervous System 

- **Full Script:** [nextflow.config](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/assembly/(CNS)%20nextflow.config?ref_type=heads)

<p>
<details>
<summary>CNS ORP Script</summary>
<pre><code>
nextflow.enable.dsl=2

process fastp {
  publishDir "$params.outdir/fastp", mode: 'copy'

  module 'fastp/0.23.2'

  memory '30 GB'
  cpus 8
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  input:
  tuple val(id), path(reads)

  output:
  tuple val(id), path('*.fastq.gz'), emit: trimmed
  tuple val(id), path('*.json'), emit: json
  tuple val(id), path('*.html'), emit: html

  script:

  """
  fastp -i ${reads[0]} -I ${reads[1]} -o ${id}_trimmed_1.fastq.gz -O ${id}_trimmed_2.fastq.gz --thread ${task.cpus} --json ${id}.json --html ${id}.html

  """
}
process ORP {
  publishDir "$params.outdir", mode: 'copy'

  memory '150 GB'
  cpus 24
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  container '/core/globus/cgi/RAMP/Scorpion_mRNA_Dec2023/scripts/orp_2.3.3.sif'

  input:
  tuple val(id), path(reads)
  val(lineage)

  output:
  path "${id}.ORP.fasta", emit: orp
  path "*/*", emit: directories

  script:
  """
  source activate orp
  /home/orp/Oyster_River_Protocol/oyster.mk STRAND=RF MEM=150 CPU=24 READ1=${reads[0]} READ2=${reads[1]} RUNOUT=${id} LINEAGE=${lineage}

  """
}
workflow {
  read_pairs_ch = Channel.fromFilePairs(params.reads, checkIfExists: true)
  fastp ( read_pairs_ch )
  ORP ( fastp.out.trimmed, params.lineage )
}

</code></pre>
</details>
</p>

### Telson 

- **Full Script:** [nextflow.config](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/assembly/(telson)%20nextflow.config?ref_type=heads)

<p>
<details>
<summary>Telson ORP Script</summary>
<pre><code>
nextflow.enable.dsl=2

process fastp {
  publishDir "$params.outdir/fastp", mode: 'copy'

  module 'fastp/0.23.2'

  memory '30 GB'
  cpus 8
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  input:
  tuple val(id), path(reads)

  output:
  tuple val(id), path('*.fastq.gz'), emit: trimmed
  tuple val(id), path('*.json'), emit: json
  tuple val(id), path('*.html'), emit: html

  script:

  """
  fastp -i ${reads[0]} -I ${reads[1]} -o ${id}_trimmed_1.fastq.gz -O ${id}_trimmed_2.fastq.gz --thread ${task.cpus} --json ${id}.json --html ${id}.html

  """
}
process ORP {
  publishDir "$params.outdir", mode: 'copy'

  memory '150 GB'
  cpus 24
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  container '/core/globus/cgi/RAMP/Scorpion_mRNA_Dec2023/scripts/orp_2.3.3.sif'

  input:
  tuple val(id), path(reads)
  val(lineage)

  output:
  path "${id}.ORP.fasta", emit: orp
  path "*/*", emit: directories

  script:
  """
  source activate orp
  /home/orp/Oyster_River_Protocol/oyster.mk STRAND=RF MEM=150 CPU=24 READ1=${reads[0]} READ2=${reads[1]} RUNOUT=${id} LINEAGE=${lineage}

  """
}
workflow {
  read_pairs_ch = Channel.fromFilePairs(params.reads, checkIfExists: true)
  fastp ( read_pairs_ch )
  ORP ( fastp.out.trimmed, params.lineage )
}

</code></pre>
</details>
</p>


## 2. Transdecoder

- **Full Script:** [TransDecoder](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/assembly/TransDecoder/TransDecoder3.sh?ref_type=heads)

<p>
<details>
<summary>Transdecoder Script</summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=transdecoder3
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=70G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=asher.coello@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load TransDecoder/5.7.0
module load hmmer/3.2.1

####STEP1: LONGEST ORF####

TransDecoder.LongOrfs -t ORP_filtered2.fasta

####STEP2: Protein Domain####
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm ORP_filtered2.fasta.transdecoder_dir/longest_orfs.pep


####STEP3: PREDICT
TransDecoder.Predict -t ORP_filtered2.fasta --no_refine_starts  --retain_pfam_hits pfam.domtblout

</code></pre>
</details>
</p>

## 3. VSEARCH 

- **Full Script:** [VSEARCH](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/assembly/VSEARCH/vsearch.sh?ref_type=heads)
<p>
<details>
<summary>VSEARCH Script</summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=8G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=asher.coello@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
module load vsearch/2.4.3
vsearch --threads 8 --log LOGFile \
        --cluster_fast /core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/ORP_filtered2.fasta.transdecoder.cds \
        --id 0.95 \
        --centroids c_stock_centroids.fasta \
        --uc clusters.uc


</code></pre>
</details>
</p>

## Transcriptome Annotation 

## 1. Alignment

- **Full Script:** [HISAT2](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/annotation/align.sh?ref_type=heads)

<p>
<details>
<summary>HISAT2 Script</summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=align
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mem=30G
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.2.1
module load samtools/1.16.1

OUTDIR=bam
mkdir -p $OUTDIR

#hisat2 index
INDEX=c_stock_index/scorp

#make list of rootnames to run
ROOTS=`ls -1 trimmed_reads/ | cut -d "_" -f 1-5 | sort | uniq`

#run hisat2
for ROOTNAME in $ROOTS
do
    echo $ROOTNAME
        hisat2 \
        -p 12 \
        -x $INDEX \
        --summary-file ${ROOTNAME}_mapping.txt \
        -1 trimmed_reads/${ROOTNAME}_1.fastq.gz \
        -2 trimmed_reads/${ROOTNAME}_2.fastq.gz | \
        samtools view -@ 12 -S -h -u - | \
        samtools sort -@ 12 -T ${ROOTNAME} - >$OUTDIR/${ROOTNAME}.bam
        # index bam files
        samtools index $OUTDIR/${ROOTNAME}.bam
    echo "finished $ROOTNAME"
done


</code></pre>
</details>
</p>

## 2. EnTAP 

- **Full Script:** [EnTAP](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20I:%20Transcriptome/annotation/EnTAP.sh?ref_type=heads)

<p>
<details>
<summary>EnTAP Script</summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=80G

module load EnTAP

EnTAP --runP --ini $PWD/entap_config.ini -i /core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/clustering/c_stock_centroids.fasta -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd --threads 12 --out-dir /core/globus/cgi/RAMP/c_stockmannorum/transcriptome/EnTAP

</code></pre>
</details>
</p>

# Part II: Chaerilus stockmannorum Genome Assembly and Annotation

## Genome Assembly 

### 1. Raw Reads 

#### Extraction 

- **Tissue Types:** Telson, Central Nervous System (CNS), and book lungs

- **Protocol:** 

##### Model
-**Flow Cell Type:**


-**Kit Type:** 

#### Nanoplot 
Nanoplot was used to generate statistics about raw long-read sequence data. 

| ONT                                                                                      | HiFi                                                                                                                  |
|:----------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------|
|![ONT_pass_pre_filter](/uploads/2010009659d946530b21f0b20c0b7d4a/ONT_pass_pre_filter.png) |![LogTransformed_HistogramReadlength](/uploads/a4e9b4c3b7169230e9ea450268a83f6b/LogTransformed_HistogramReadlength.png)|





|Raw Reads (PASS)                                 |          ONT    |      Hifi         |  
| ------------------------------------------------|:---------------:|:-----------------:|
| Mean Read Length                                | 3,584.30        |        9,250.2    |  
| Mean Read Quality                               |  20.1           |     31.2          |
| Median Read Length                              | 1,198.00        | 8,195.0           | 
| Median Read Quality                             | 20.2            |         32.6      |
| Number of Reads                                 | 8,370,464.00    |  8,346,096.0      |
| Read Length N50                                 |8,450.00         |   11,188.0        |
| Total Bases                                     |30,002,509,670.00|77,202,676,464.0   |

#### Genome Size Estimation


<p>
<details>
<summary> Genome Size Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=kmerfreq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G

#calculate k-mer frequency using kmerfreq
ls /core/globus/cgi/RAMP/c_stockmannorum/assembly/01_raw_reads/cstock.fastq.gz > read_files.lib
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 10 read_files.lib

#extract k-mer individual number
less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum

#num is the Kmer indivdual number
num=$(awk 'NR == 3 {print $4}' read_files.lib.kmer.freq.stat)

#run GCE in heterozygous mode
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g $num -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log

</code></pre>
</details>
</p>

### 2. Contamination Removal 
---------------------------------------------------------------------------------------------------------------

#### Centrifuge
Centrifuge is a bioinformatic tool that decontaminates the raw data by detecting fungal, bacterial, archael, and viral sequences and removing them. Nanoplot was used again after centrifuge to visualize how many contaminants were removed. 

**Top 3 Contaminants (Database: f+b+a+v)**

1. _Porphyrobacter sp. GA68_
2. _Pseudomonas sp. CIP-10_
3. _Mesorhizobium sp. Pch-S_

|ONT                                                                                                                        |HiFi                                                                                                                   |
|--------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------|
|![Screenshot_2024-05-20_224952](/uploads/86488ae71f05fcc930e1b4acb14c1914/Screenshot_2024-05-20_224952.png)                |![Screenshot_2024-05-20_225735](/uploads/a7977c2215d37fd84104d16c30bc7bcd/Screenshot_2024-05-20_225735.png)            |



**Nanoplot**
|ONT                                                                                                                        |HiFi                                                                                                                   |
|--------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------|
|![LogTransformed_HistogramReadlength-2](/uploads/ffddc17c401967b852ded0961f4d4ceb/LogTransformed_HistogramReadlength-2.png)|![LogTransformed_HistogramReadlength](/uploads/878ce6ec98a4286eecdc5b9e721bb36e/LogTransformed_HistogramReadlength.png)|

 
|After Filtering                                  |   ONT           |     HiFi        |               
|------------------------------------------------:|:---------------:|:---------------:|
| Mean Read Length                                |3,584.3          | 9,249.3         |                     
| Mean Read Quality                               |18.2             |31.2             |                   
| Median Read Length                              | 1,198.0         |8,193.0          |                    
| Median Read Quality                             |18.4             |32.6             |                   
| Number of Reads                                 | 8,370,464.0     |8,326,535.0      |                   
| Read Length N50                                 | 8,450.0         |11,186.0         |                   
| Total Bases                                     |30,002,509,670.0 |77,014,292,943.0 |                  


- **Full Script:** [Centrifuge Script](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/tree/main/Part%20II:%20Genome/assembly/02_remove_contaminants?ref_type=heads)

<p>
<details>
<summary> Centrifuge + Nanoplot Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=centrifuge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=60G
#SBATCH --mail-user=jennifer.santiago_membreno@uconn.edu,adam.glendening@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load centrifuge/1.0.4-beta

dir=/core/globus/cgi/RAMP/c_stockmannorum/assembly/01_raw_reads
centrifuge -q \
        -x /core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v/abv \
        --report-file centrifuge_report.tsv \
        --quiet \
        --min-hitlen 50 \
        -p 12 \
        -U $dir/cstock.fastq.gz
        -S centrifuge.out

module load seqkit/2.2.0
#taking the classified reads from the STDOUT file and adding those to a new file
grep -vw "unclassified" centrifuge.out > contaminated_reads.txt
#extracting just the IDs from the text file
awk NF=1 contaminated_reads.txt > contaminated_read_ids.txt
#keeping only unique IDs and removing any duplicates
sort -u contaminated_read_ids.txt > no_dup_centrifuge_contaminated_read_ids.txt
#adds only the sequences that DO NOT match the headers in the file we provided
seqkit grep -v -f no_dup_centrifuge_contaminated_read_ids.txt $dir/cstock.fastq.gz  > cstock_filtered.fastq

module load NanoPlot/1.33.0

#Centrifuge filtered
NanoPlot --fastq cstock_filtered.fastq \
        --loglength \
        -o summary_filtered \
        -t 12

</code></pre>
</details>
</p>

#### Genome Size Estimation Post Filtering
<p>
<details>
<summary> Genome Size Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=kmerfreq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G

#calculate k-mer frequency using kmerfreq
ls /core/globus/cgi/RAMP/c_stockmannorum/assembly/02_quality_control/cstock_filtered.fastq > read_files.lib
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 16 read_files.lib

#extract k-mer individual number
less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum

#num is the Kmer indivdual number
num=$(awk 'NR == 3 {print $4}' read_files.lib.kmer.freq.stat)

#run GCE in heterozygous mode
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g $num -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log

</code></pre>
</details>
</p>


### 3. Draft Genome Assemblies
------------------------------------------------------------------------------------------------------------------------------

### Hifiasm
Hifiasm is an assembler that produces high quality, haplotype-resolved assemblies.

- **Full Script:** [Hifiasm](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20II:%20Genome/assembly/03_assembly%20/hifiasm.sh?ref_type=heads)

<p>
<details>
<summary> Hifiasm Script </summary>
<pre><code>

#!/bin/bash
#BATCH --job-name=hifiasm_hybrid
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --mail-type=END
#SBATCH --mail-user=jennifer.santaigo_membreno@uconn.edu
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=200G
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err


hifiasm=/core/projects/EBP/conservation/software/hifiasm/hifiasm
reads=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/01_centrifuge/cstock_filtered.fastq

$hifiasm -o scorpion2.asm --primary -t 32 --ul /core/globus/cgi/RAMP/c_stockmannorum/assembly/02_quality_control/cstock_filtered.fastq $reads

</code></pre>
</details>
</p>

#### Metrics

- **Full Scripts:** [here](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/tree/main/Part%20II%3A%20Genome/assembly/03_assembly%20?ref_type=heads)

**BUSCO**

BUSCO is a program that estimates the completeness of an assembly by comparing provided sequences against a set of highly conserved single-copy genes in a particular species.  

| BUSCO                               |             |
|-------------------------------------|-------------|
| Complete BUSCOs (C)                 | 996 (98.3%) |
| Complete and single-copy BUSCOs (S) | 914 (90.2%) |
| Complete and duplicated BUSCOs (D)  | 82 (8.1%)   |
| Fragmented BUSCOs (F)               | 9 (0.9%)    |
| Missing BUSCOs (M)                  | 8 (0.8%)    |
| Total BUSCO groups searched         | 1013        |


<p>
<details>
<summary> BUSCO Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 15
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err

module load busco/5.4.5

dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly
busco -i $dir/scorpion2.asm.p_ctg.fa -o flye_busco -l arthropoda -m genome -c 15 -r

</code></pre>
</details>
</p>

**Merqury**

Merqury is a tool that evaluates the accuracy of genome assemblies by breaking input reads into shorter sub-sequences and aligning them back to the genome to identify errors. The Quality Value (QV) score reflects the base-calling quality and the error score is a measure of assembly correctness. 


| MERQURY |   |
|---------|---|
| QV      |   |
| Error   |   |

<p>
<details>
<summary>Merqury Script </summary>
<pre><code>
#!/bin/bash
#SBATCH --job-name=merqury
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mem=120G
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load meryl/1.3
module load singularity

ONT_dir=/core/globus/cgi/RAMP/c_stockmannorum/assembly/02_quality_control
Hifi_dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly
assembly=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly

#Run Meryl
#Step 1: Build meryl db for each input type
meryl k=21 count output read_hifi.meryl $Hifi_dir/hifi.fastq
meryl k=21 count output read_ONT.meryl $ONT_dir/cstock_filtered.fastq

#Step 2: Merge
meryl union-sum output scorpion.meryl read*.meryl

#Run Merqury
singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif merqury.sh scorpion.meryl $assembly/scorpion2.asm.p_ctg.fa merqury

</code></pre>
</details>
</p>

**Quast**

Quast is a quality assessment tool that it used to assess contiguity of assemblies. It is useful for comparing genome assemblies. 

| Quast          |            |
|----------------|------------|
| # contigs      | 2567       |
| Largest contig | 52306469   |
| Total Length   | 3803132997 |
| GC (%)         | 34.86      |
| N50            | 7764390    |

<p>
<details>
<summary> Quast Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mem=50G
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err

module load quast/5.2.0
dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly

quast.py --threads 10 -o flye_quast $dir/scorpion2.asm.p_ctg.fa

</code></pre>
</details>
</p>

### 4. Purging Haplotigs 
----------------------------------------------------------------------------------------------------------------------------------------
Purge Haplotigs is a tool that identifies and removes duplicate haplotype contigs from genome assemblies. After purge, the assembly was filtered to remove fragments shorter than 3kb.

![HiFi.bam.histogram](/uploads/f21f019b5efb79b650ea9b8622ad9fdd/HiFi.bam.histogram.png)


- **Full Scripts:** [here](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/tree/main/Part%20II%3A%20Genome/assembly/04_purge_haplotigs?ref_type=heads)

<p>
<details>
<summary> Purge Haplotigs Script </summary>
<pre><code>

#!/bin/bash
#BATCH --job-name=purge_haplotigs
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=180G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load minimap2/2.26
#module load samtools/1.16.1

#path to draft assembly
ref=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly/scorpion2.asm.p_ctg.fa
#path to raw reads
#read_file=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/hifi.fastq

#minimap2 -t 16 -ax map-ont $ref $read_file > flye_coverage.sam
#samtools view -b flye_coverage.sam -@ 16 | samtools sort -@ 16 -o flye_coverage.bam -T flye_coverage_tmp.ali
#samtools index flye_coverage.bam
#rm flye_coverage.sam

##########################################
##purge haplotigs                 ##
##########################################
module load purge_haplotigs/1.1.2
module load R/4.2.2

##STEP-1
#purge_haplotigs readhist -b HiFi.bam -g $ref -t 16

##STEP-2 - change l, m, h based on histogram output from previous script
#purge_haplotigs cov -i HiFi.bam.gencov -l 7 -m 21 -h 195

##STEP-3
purge_haplotigs purge -b HiFi.bam -g $ref -c coverage_stats.csv -d -t 16

</code></pre>
</details>
</p>

<p>
<details>
<summary> Filter Post Purge Script </summary>
<pre><code>
#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=BEGIN,END
#SBARCH --mail-user=jennifer.santiago_membreno@uconn.edu
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
module load seqkit
genome=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/03_purge/curated.fasta
seqkit seq -m 3000 $genome | seqkit sort --by-length --reverse | seqkit replace --pattern '.+' --replacement 'Cstock_scaffold_{nr}' > c_stockmannorum_hybrid.fasta

</code></pre>
</details>
</p>

#### Metrics

**BUSCO**

| BUSCO                               |             |
|-------------------------------------|-------------|
| Complete BUSCOs (C)                 | 995 (98.2%) |
| Complete and single-copy BUSCOs (S) | 919 (90.7%) |
| Complete and duplicated BUSCOs (D)  | 76 (7.5%)   |
| Fragmented BUSCOs (F)               | 9 (0.9%)    |
| Missing BUSCOs (M)                  | 9 (0.9%)    |
| Total BUSCO groups searched         | 1013        |


<p>
<details>
<summary> BUSCO Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 15
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err

module load busco/5.4.5

dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/03_purge
busco -i $dir/curated.fasta -o flye_busco -l arthropoda -m genome -c 15 -r
</code></pre>
</details>
</p>

**Merqury**

| MERQURY |   |
|---------|---|
| QV      |   |
| Error   |   |

<p>
<details>
<summary> Merqury Script </summary>
<pre><code>
#!/bin/bash
#SBATCH --job-name=merqury
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

module load meryl/1.3
module load singularity

dir=/core/globus/cgi/RAMP/c_stockmannorum/assembly/02_quality_control/cstock_filtered.fastq /core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/hifi.fastq        
#assembly=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly

meryl k=21 count output scorpion2.meryl $dir

singularity exec /isg/shared/databases/nfx_singularity_cache/merqury.sif merqury.sh scorpion.meryl $assembly/assembly.fasta merqury
</code></pre>
</details>
</p>

**Quast**

| Quast          |            |
|----------------|------------|
| # contigs      | 895        |
| Largest contig | 52306469   |
| Total Length   | 3322096980 |
| GC (%)         | 34.78      |
| N50            | 9775463    |


<p>
<details>
<summary> Quast Script </summary>
<pre><code>
#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mem=50G
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load quast/5.2.0
dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/03_purge

quast.py --threads 10 -o purge_quast $dir/curated.fasta

</code></pre>
</details>
</p>


## Annotation 
Genome Annotation provides insights into gene locations, structures, and function by comparing known sequences to the genome assembly. Uncharacterized sequences and their functions can be predicted by comparing them to related sequences.

### 1. Soft-Masked Genome
----------------------------------------------------------------------------------------------------------------------------------------
#### Indexing
The first step in the annotation process is indexing the final genome assembly which creates a reference file that allows rapid retrieval of information from the genome. 

- **Full Script:** [Index](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20II:%20Genome/annotation/01_genome/1_build_database.sh?ref_type=heads)

<p>
<details>
<summary> Index Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=repeat_db
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=70G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.9.2

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif BuildDatabase -name "rm_database" /core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/01_genome/c_stockmannorum_hybrid.fasta

</code></pre>
</details>
</p>

#### Repeat Modeler
Repeat modeler identifies repetitive sequences from the genome assembly and creates a consensus repeat library.

- **Full Script:** [Repeat Modeler](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20II:%20Genome/annotation/01_genome/2_repeat_modeler.sh?ref_type=heads)


<p>
<details>
<summary> Repeat Modeler Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=repeat_modeler
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=jennifer.santiago_membreno@uconn.edu
#SBATCH --mem=70G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.9.2
singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatModeler -threads 30 -database rm_database

</code></pre>
</details>
</p>

#### Repeat Masker
Repeat masker uses the `consensi.fa.classified` to mask the repetitve elements in the genome assembly, which completes the process of soft-masking the genome

- **Full Script:** [Repeat Masker](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/blob/main/Part%20II:%20Genome/annotation/01_genome/3_repeat_masker.sh?ref_type=heads)

<p>
<details>
<summary> Repeat Masker Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=repeat_masker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.9.2

genome=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/01_genome/

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatMasker \
 -dir repeatmasker_out -pa 16 \
 -lib RM_*/consensi.fa.classified \
 -gff -a -noisy -xsmall $genome/c_stockmannorum_hybrid.fasta

</code></pre>
</details>
</p>

### 2. EASEL 
----------------------------------------------------------------------------------------------------------------------------------------


#### EASEL Annotation Statistics 

**No OrthoDB**
|    EASEL            | Unfiltered | Filtered|Filtered (Longest Isoform)|
|---------------------|------------|---------|--------------------------|
| BUSCO (% Complete)  | 98.4%      | 94.9%   | 92.6%                    |
| # of genes          | 36349      | 13896   | 13896                    |
| Mono:Multi          | 0.66       | 0.07    | 0.07                     |
|EnTAP Alignment Rate | 0.59       | 0.81    | 0.75                     |

**With OrthoDB**
|    EASEL            | Unfiltered | Filtered|Filtered (Longest Isoform)|
|---------------------|------------|---------|--------------------------|
| BUSCO (% Complete)  | 99.1%      | 97.1%   | 92.6%                    |
| # of genes          | 58595      | 14613   | 14613                    |
| Mono:Multi          | 1.02       | 0.07    | 0.06                     |
|EnTAP Alignment Rate | 0.63       | 0.81    | 0.73                     |

- **Full Scripts:** [here](https://gitlab.com/PlantGenomicsLab/chaerilus-stockmannorum-genomics/-/tree/main/Part%20II:%20Genome/annotation/02_EASEL?ref_type=heads)

<p>
<details>
<summary> EASEL Nextflow Script </summary>
<pre><code>

#!/bin/bash
#SBATCH --job-name=stock
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-type=ALL

module load nextflow/22.10.6
module load singularity

SINGULARITY_TMPDIR=/core/labs/Wegrzyn/easel
export SINGULARITY_TMPDIR

nextflow run -hub gitlab PlantGenomicsLab/easel -params-file stock.yaml -profile singularity,xanadu -queue-size 10

</code></pre>
</details>
</p>

<p>
<details>
<summary> stock.yaml </summary>
<pre><code>
outdir    : "stock_easel"
genome : "/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/02_softmask/repeatmasker_out/c_stockmannorum_hybrid.fasta.masked"
busco_lineage   : "arthropoda"
order : "Arthropoda"
prefix     : "stock"
reference_db : "/isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd"
taxon : "chelicerata"
singularity_cache_dir: "/isg/shared/databases/nfx_singularity_cache"
augustus: "/home/FCAM/jesantiago"
training_set : 'invertebrate'
bam : '/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/03_easel/stock_easel/03_alignments/bam/*.bam'
rate : 70
resume_filtering: true
external_protein: false
rna_list: "/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/03_easel/no_orthodb/rna.txt"
augustus_list: "/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/03_easel/no_orthodb/augustus.txt"
</code></pre>
</details>
</p>

### 3. EnTAP Functional Annotation



## Contributing

## Authors and acknowledgment


## License


