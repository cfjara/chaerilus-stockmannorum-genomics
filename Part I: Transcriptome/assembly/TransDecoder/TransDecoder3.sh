#!/bin/bash
#SBATCH --job-name=transdecoder3
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=70G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=asher.coello@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load TransDecoder/5.7.0
module load hmmer/3.2.1

####STEP1: LONGEST ORF####

TransDecoder.LongOrfs -t ORP_filtered2.fasta

####STEP2: Protein Domain####
hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm ORP_filtered2.fasta.transdecoder_dir/longest_orfs.pep


####STEP3: PREDICT
TransDecoder.Predict -t ORP_filtered2.fasta --no_refine_starts  --retain_pfam_hits pfam.domtblout
