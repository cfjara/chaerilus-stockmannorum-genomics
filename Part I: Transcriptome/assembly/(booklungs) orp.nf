nextflow.enable.dsl=2

process fastp {
  publishDir "$params.outdir/fastp", mode: 'copy'

  module 'fastp/0.23.2'

  memory '30 GB'
  cpus 8
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  input:
  tuple val(id), path(reads)

  output:
  tuple val(id), path('*.fastq.gz'), emit: trimmed
  tuple val(id), path('*.json'), emit: json
  tuple val(id), path('*.html'), emit: html

  script:

  """
  fastp -i ${reads[0]} -I ${reads[1]} -o ${id}_trimmed_1.fastq.gz -O ${id}_trimmed_2.fastq.gz --thread ${task.cpus} --json ${id}.json --html ${id}.html

  """
}
process ORP {
  publishDir "$params.outdir", mode: 'copy'

  memory '150 GB'
  cpus 24
  executor 'slurm'
  clusterOptions '--qos=general'
  queue 'general'

  container '/core/globus/cgi/RAMP/Scorpion_mRNA_Dec2023/scripts/orp_2.3.3.sif'

  input:
  tuple val(id), path(reads)
  val(lineage)

  output:
  path "${id}.ORP.fasta", emit: orp
  path "*/*", emit: directories

  script:
  """
  source activate orp
  /home/orp/Oyster_River_Protocol/oyster.mk STRAND=RF MEM=150 CPU=24 READ1=${reads[0]} READ2=${reads[1]} RUNOUT=${id} LINEAGE=${lineage}

  """
}
workflow {
  read_pairs_ch = Channel.fromFilePairs(params.reads, checkIfExists: true)
  fastp ( read_pairs_ch )
  ORP ( fastp.out.trimmed, params.lineage )
}
