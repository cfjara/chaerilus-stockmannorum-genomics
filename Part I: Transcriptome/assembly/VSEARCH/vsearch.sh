#!/bin/bash
#SBATCH --job-name=vsearch
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=8G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=asher.coello@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
module load vsearch/2.4.3
vsearch --threads 8 --log LOGFile \
        --cluster_fast /core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/ORP_filtered2.fasta.transdecoder.cds \
        --id 0.95 \
        --centroids c_stock_centroids.fasta \
        --uc clusters.uc
