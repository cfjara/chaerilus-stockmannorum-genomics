#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH --mail-user=asher.coello@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load quast/5.2.0
dir=/core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/clustering/c_stock_centroids.fasta

quast.py --threads 10 -o c_stock_centroids_quast $dir
