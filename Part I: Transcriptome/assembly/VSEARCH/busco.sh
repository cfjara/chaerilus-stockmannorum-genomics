#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 15
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=all
#SBATCH --mail-user=asher.coello@uconn.edu
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

hostname
date

export TMPDIR=$PWD/tmp

module load busco/5.4.5

dir=/core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/clustering/
busco -i $dir/c_stock_centroids.fasta -o busco_centroids -l arthropoda -m transcriptome -c 15
