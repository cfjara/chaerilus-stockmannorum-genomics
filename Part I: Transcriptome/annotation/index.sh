#!/bin/bash
#SBATCH --job-name=index
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G
#SBATCH --mail-type=END
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.2.1
txdir=/core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/clustering
outdir=c_stock_index

hisat2-build $txdir/c_stock_centroids.fasta $outdir/scorp

module unload hisat2/2.2.1
