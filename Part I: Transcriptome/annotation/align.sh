#!/bin/bash
#SBATCH --job-name=align
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mem=30G
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.2.1
module load samtools/1.16.1

OUTDIR=bam
mkdir -p $OUTDIR

# hisat2 index
INDEX=c_stock_index/scorp

#make list of rootnames to run
ROOTS=`ls -1 trimmed_reads/ | cut -d "_" -f 1-5 | sort | uniq`

# run hisat2
for ROOTNAME in $ROOTS
do
    echo $ROOTNAME
        hisat2 \
        -p 12 \
        -x $INDEX \
        --summary-file ${ROOTNAME}_mapping.txt \
        -1 trimmed_reads/${ROOTNAME}_1.fastq.gz \
        -2 trimmed_reads/${ROOTNAME}_2.fastq.gz | \
        samtools view -@ 12 -S -h -u - | \
        samtools sort -@ 12 -T ${ROOTNAME} - >$OUTDIR/${ROOTNAME}.bam
        # index bam files
        samtools index $OUTDIR/${ROOTNAME}.bam
    echo "finished $ROOTNAME"
done
