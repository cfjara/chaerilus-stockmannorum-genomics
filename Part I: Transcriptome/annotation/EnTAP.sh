#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=80G

module load EnTAP

EnTAP --runP --ini $PWD/entap_config.ini -i /core/globus/cgi/RAMP/c_stockmannorum/transcriptome/combined/clustering/c_stock_centroids.fasta -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd --threads 12 --out-dir /core/globus/cgi/RAMP/c_stockmannorum/transcriptome/EnTAP
