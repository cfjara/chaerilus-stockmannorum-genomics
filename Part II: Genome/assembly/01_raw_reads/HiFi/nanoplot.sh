#!/bin/bash
#SBATCH --job-name=Nanoplot
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G


module load NanoPlot/1.33.0

NanoPlot --fastq  hifi.fastq \
        --loglength \
        -o summary_filtered \
        -t 10