#!/bin/bash
#SBATCH --job-name=bam2fastq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

source activate pbtk

bam2fastq -o hifi /core/globus/cgi/RAMP/c_stockmannorum/HiFi_reads/1_C01/hifi_reads/m84122_240202_202554_s3.hifi_reads.bam

gzip -d hifi.fastq.gz
chmod 777 hifi.fastq