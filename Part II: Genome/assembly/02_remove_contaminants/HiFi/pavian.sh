#!/bin/bash
#SBATCH --job-name=pavian_report
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=carolina.jara@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load centrifuge/1.0.4-beta
index=/core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v

centrifuge-kreport -x $index/abv /core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/01_centrifuge/centrifuge_report.tsv > pavian_report_shear.out