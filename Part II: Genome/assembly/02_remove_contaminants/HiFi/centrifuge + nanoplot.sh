#!/bin/bash
#SBATCH --job-name=centrifuge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=150G

#module load centrifuge/1.0.4-beta

dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/hifi.fastq
#centrifuge -q \
#        -x /core/projects/EBP/Wegrzyn/tsuga/adelges/02_quality_control/abfpv \
#        --report-file centrifuge_report.tsv \
#       --quiet \
#        --min-hitlen 50 \
#        -p 12 \
#        -U $dir \
#        -S centrifuge.out

module load seqkit/2.2.0
#taking the classified reads from the STDOUT file and adding those to a new file
#grep -vw "unclassified" centrifuge.out > contaminated_reads.txt
#extracting just the IDs from the text file
#awk NF=1 contaminated_reads.txt > contaminated_read_ids.txt
#keeping only unique IDs and removing any duplicates
#sort -u contaminated_read_ids.txt > no_dup_centrifuge_contaminated_read_ids.txt
#adds only the sequences that DO NOT match the headers in the file we provided
seqkit grep -v -f no_dup_centrifuge_contaminated_read_ids.txt $dir  > cstock_filtered.fastq

module load NanoPlot/1.33.0

#Centrifuge filtered
NanoPlot --fastq cstock_filtered.fastq \
        --loglength \
        -o summary_filtered \
        -t 12