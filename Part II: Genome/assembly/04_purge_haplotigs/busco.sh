#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 15
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err

module load busco/5.4.5

dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/03_purge
busco -i $dir/curated.fasta -o flye_busco -l arthropoda -m genome -c 15 -r