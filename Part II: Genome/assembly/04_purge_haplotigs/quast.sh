#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mem=50G
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load quast/5.2.0
dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/03_purge

quast.py --threads 10 -o purge_quast $dir/curated.fasta