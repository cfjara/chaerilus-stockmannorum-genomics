#SBATCH --job-name=filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=BEGIN,END
#SBARCH --mail-user=jennifer.santiago_membreno@uconn.edu
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
module load seqkit
genome=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/03_purge/curated.fasta
seqkit seq -m 3000 $genome | seqkit sort --by-length --reverse | seqkit replace --pattern '.+' --replacement 'Cstock_scaffold_{nr}' > c_stockmannorum_hybrid.fasta
