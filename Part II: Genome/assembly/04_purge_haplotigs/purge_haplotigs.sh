#!/bin/bash
#BATCH --job-name=purge_haplotigs
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=180G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load minimap2/2.26
#module load samtools/1.16.1

#path to draft assembly
ref=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly/scorpion2.asm.p_ctg.fa
#path to raw reads
#read_file=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/hifi.fastq

#minimap2 -t 16 -ax map-ont $ref $read_file > flye_coverage.sam
#samtools view -b flye_coverage.sam -@ 16 | samtools sort -@ 16 -o flye_coverage.bam -T flye_coverage_tmp.ali
#samtools index flye_coverage.bam
#rm flye_coverage.sam

##########################################
##      purge haplotigs                 ##
##########################################
module load purge_haplotigs/1.1.2
module load R/4.2.2

## STEP-1
#purge_haplotigs readhist -b HiFi.bam -g $ref -t 16

## STEP-2 - change l, m, h based on histogram output from previous script
#purge_haplotigs cov -i HiFi.bam.gencov -l 7 -m 21 -h 195

## STEP-3
purge_haplotigs purge -b HiFi.bam -g $ref -c coverage_stats.csv -d -t 16