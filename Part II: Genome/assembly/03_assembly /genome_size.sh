#!/bin/bash
#SBATCH --job-name=kmerfreq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mem=50G
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

# calculate k-mer frequency using kmerfreq
ls /core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/01_centrifuge/cstock_filtered.fastq > read_files.lib
/core/projects/EBP/conservation/software/kmerfreq/kmerfreq -k 17 -t 16 read_files.lib

# extract k-mer individual number
less read_files.lib.kmer.freq.stat | grep "#Kmer indivdual number"
less read_files.lib.kmer.freq.stat | perl -ne 'next if(/^#/ || /^\s/); print; ' | awk '{print $1"\t"$2}' > read_files.lib.kmer.freq.stat.2colum

# num is the Kmer indivdual number
num=$(awk 'NR == 3 {print $4}' read_files.lib.kmer.freq.stat)

# run GCE in heterozygous mode
/core/projects/EBP/conservation/software/GCE/gce-1.0.2/gce -g $num -f read_files.lib.kmer.freq.stat.2colum  >gce2.table 2>gce2.log