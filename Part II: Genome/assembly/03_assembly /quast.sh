#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END # mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mem=50G
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err

module load quast/5.2.0
dir=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/02_assembly

quast.py --threads 10 -o flye_quast $dir/scorpion2.asm.p_ctg.fa