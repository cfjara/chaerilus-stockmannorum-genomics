#!/bin/bash
#BATCH --job-name=hifiasm
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --mail-type=END
#SBATCH --mail-user=jennifer.santaigo_membreno@uconn.edu
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=200G
#SBATCH -o myscript_%j.out
#SBATCH -e myscript_%j.err


hifiasm=/core/projects/EBP/conservation/software/hifiasm/hifiasm
reads=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_assembly/01_centrifuge/cstock_filtered.fastq

$hifiasm -o scorpion.asm --primary -t 32 $reads