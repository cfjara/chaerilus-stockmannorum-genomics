#!/bin/bash
#SBATCH --job-name=stock
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-type=ALL

module load nextflow/22.10.6
module load singularity

SINGULARITY_TMPDIR=/core/labs/Wegrzyn/easel
export SINGULARITY_TMPDIR

nextflow run -hub gitlab PlantGenomicsLab/easel -params-file stock.yaml -profile singularity,xanadu -queue-size 10