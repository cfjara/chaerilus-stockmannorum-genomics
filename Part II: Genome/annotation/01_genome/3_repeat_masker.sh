
#!/bin/bash
#SBATCH --job-name=repeat_masker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=samuel.pring@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.9.2

genome=/core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/01_genome/

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatMasker \
 -dir repeatmasker_out -pa 16 \
 -lib RM_*/consensi.fa.classified \
 -gff -a -noisy -xsmall $genome/c_stockmannorum_hybrid.fasta