#!/bin/bash
#SBATCH --job-name=repeat_db
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=70G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity/3.9.2

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif BuildDatabase -name "rm_database" /core/globus/cgi/RAMP/c_stockmannorum/HiFi_annotation/01_genome/c_stockmannorum_hybrid.fasta